<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp-html-to-wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '_h3?rjiWZ!.NsHpP]8G1:@i|XG@bV@d0+]f|0g 8^4H9H@umI}58d`%CXLB+F|m0' );
define( 'SECURE_AUTH_KEY',  '){bd%*> :|<9gP/^pc1VyV`XHD9%ujlV2JV];S{y_@eduT~f!1R):7~I+v %zFJI' );
define( 'LOGGED_IN_KEY',    'Z<gpY/J8q6d!} kQ8hJq&+wWsK=]}3;KG!Ev%s].Pc^`^FRT8qLjLIxXaz:E=lp3' );
define( 'NONCE_KEY',        'SO$H<,8.y%n!=+rX,}RioGUN-Gnxv[)0ICsxqJF^U8HCbD,!4gPakvSLnL)0xj+>' );
define( 'AUTH_SALT',        'nk)lkXK.tMx^jG:+#?[@&*XEDjdJ%|3.-@d)J}#+yX>dGafv_[Fp4$43L8:~H;D6' );
define( 'SECURE_AUTH_SALT', ',XJ{fz3U/-`RgZ`0u.o}.m5F}[l_g@H.qZ&_Lt,[pn3p|1~_DvywH,nm#[%84Ok/' );
define( 'LOGGED_IN_SALT',   'o$wa[o=Q:N(a#ZDqzc?eW(u1?vBmL{a1su2uQ<.Xv>#[ZCRir{OTXZV$#XJ2iEDT' );
define( 'NONCE_SALT',       'wRYu,AM%Uk/Vu]Qo2hK!~wnFk}tMl^JRJ=:k5dv.b*pp`:99|B@u(?|S8ezS#}&e' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
